#!/bin/sh

# simple argument check
if [ -z "$1" ] || [ ! -d "$1" ]; then
  echo "Usage: $0 <config_dir>"
  exit 1
fi

# loading config
bootspec="$1/boot.json"
entries="$(ls /nix/var/nix/profiles/ | grep -e 'system-[0-9]*-link' | sort -n -t '-' -k 2)"

# common config
timeout="$(jq '.v1.extension.org.crans.timeout' "$bootspec")"

# efi config
efiSysMountPoints="$(    jq '.v1.extension.org.crans.efi.efiSysMountPoints' "$bootspec")"
canTouchEfiVariables="$( jq '.v1.extension.org.crans.efi.canTouchEfiVariables' "$bootspec")"

# systemd-boot config
editor="$(               jq '.v1.extensions.org.crans."systemd-boot".editor' "$bootspec")"
gracefull="$(            jq '.v1.extensions.org.crans."systemd-boot".graceful' "$bootspec")"
extraFiles="$(           jq '.v1.extensions.org.crans."systemd-boot".extraFiles' "$bootspec")"
consoleMode="$(          jq '.v1.extensions.org.crans."systemd-boot".consoleMode' "$bootspec")"
extraEntries="$(         jq '.v1.extensions.org.crans."systemd-boot".extraEntries' "$bootspec")"
configurationLimit="$(   jq '.v1.extensions.org.crans."systemd-boot".configurationLimit' "$bootspec")"
extraInstallCommands="$( jq '.v1.extensions.org.crans."systemd-boot".extraInstallCommands' "$bootspec")"

# create a tmp dir to build and collect files.
tmp="$(mktemp -d -t 'systemd-boot-XXXXXXXXX')"
mkdir -p "$tmp/loader/entries"

# generate loader.conf
echo "# Generated file; DO NOT EDIT
title Nixos Bootloader
timeout $timeout
console-mode $consoleMode
editor $editor
auto-entries false
auto-firmware true
random-seed-mode with-system-token
default nixos-configuration-*
" > "$tmp/loader/loader.conf"

# generation generation-${n} for each entries.
# TODO: flatten specialisations into entries.
mkdir -p "$tmp/EFI/nixos"
for entry in $entries
do
  # loading entry config
  init="$(         jq '.v1.init' "$entry/boot.json")"
  initrd="$(       jq '.v1.initrd' "$entry/boot.json")"
  kernel="$(       jq '.v1.kernel' "$entry/boot.json")"
  kernelParams="$( jq '.v1.kernelParams' "$entry/boot.json")"
  label="$(        jq '.v1.label' "$entry/boot.json")"
  
  # symlinking kernel and initrd if they are not already symlinked.
  kernelName="$(echo "$kernel" | cut -d'/' -f4)"
  if [ ! -f "$kernelName.efi" ]; then
    ln "$kernel" "$tpm/EFI/nixos/$kernelName.efi"
  fi

  initrdName="$(echo "$kernel" | cut -d'/' -f4)"
  if [ ! -f "$initrdName.efi" ]; then 
    ln "$initrd" "$tpm/EFI/nixos/$initrdName.efi"
  fi

  # generating nixos-generation-${n}.conf
  gen_number="$(echo "$entry" | cut -d'-' -f2)"
  echo "
  title NixOS
  version Generation $gen_number $label
  linux /EFI/nixos/$kernel
  initrd /EFI/nixos/$initrd
  options init=$init $kernelParams
  " > "$tmp/loader/entries/nixos-generation-$gen_number.conf"
done

# installing systemd.
for esp in $efiSysMountPoints; do
  bootctl --esp-path="$esp" install
  if [ ! -d "$esp/EFI/nixos" ]; then
    mkdir "$esp/EFI/nixos"
  fi
  rm "$esp/EFI/nixos/"*
  rm "$esp/loader/entries/"*
  cp "$tmp/EFI/nixos/"* "$esp/EFI/nixos/"
  cp "$tmp/loader/loader.conf" "$esp/loader/loader.conf"
  cp "$tmp/loader/entries/"* "$esp/loader/entries/"
done
# cleaning up artefacts
rm -rf "$tmp"