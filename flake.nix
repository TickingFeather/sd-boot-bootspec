{
  description = "Try at implementing secure boot with systemd for bootspec.";

  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.nixpkgs.url = "nixpkgs/nixos-unstable";

  outputs = {
    self,
    nixpkgs,
    flake-utils,
  }:
    flake-utils.lib.eachSystem (["x86_64-linux"]) (
      system: let
        pkgs = nixpkgs.legacyPackages.${system};
      in {
        formatter = pkgs.alejandra;
        devShell = pkgs.mkShell {
          buildInputs = [
            pkgs.sbctl
          ];
        };
        nixosModules.sd-boot = ./modules/sd-boot/sd-boot.nix;
      }
    );
}
